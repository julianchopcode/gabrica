<html>
    <head>
        <link rel="stylesheet" href="public/css/app.css">
    </head>
    <title>
        Landing page
    </title>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-right">
                    <img src="public/img/ganate.png">
                    <div class="text-center color-white">
                        <h1>6 MESES DE SNACKS</h1>
                        <h3>PARA TU MASCOTA</h3>
                    </div>
                    <img src="public/img/img-snacks.png">
                    <div class="text-center color-white">
                        <p>El bienestar para tu mascota es lo más importante para Gabrica.
                        Participa para ganar 6 meses de snacks de Cat Licious y Dog Licious</p>
                    </div>
                    <div class="text-center color-white">
                        <h2>¡Y consiéntelos con lo que más les gusta a ellos!</h2>
                    </div>
                </div>
                <div class="col-6 text-left">
                    <img class="img-gabrica" src="public/img/logo-gabrica.png" alt="">
                    <form class="form-color" action="form.php" method="POST">
                        <h1 class="color-violet">Completa tus datos</h1>
                        <div class="form-group">
                          <input type="text" name = "nombre" class="form-control" id="" placeholder="Nombre Completo" required>
                          <br>
                          <input type="text" name ="telefono" class="form-control" id="" placeholder="Teléfono" required>
                          <br>
                          <input type="email" name = "email" class="form-control" id="" placeholder="Email" required>
                          <br>
                        </div>
                        <input class="color-enviar" type="submit" value="QUIERO PARTICIPAR">
                        <br>
                      </form>
                </div>
            </div>
            <div class="row-full">
                <div class="text-center">
                    <img src="public/img/como-ganar.png">
                </div>
                <div class="tex-center color-white">
                    <div class="text-footer">
                        <h3>Premiamos a las 2 respuestas más acertadas y creativas sobre <br>
                            ¿Cuál es la mejor manera que acostumbras a premiar a tu mascota?</h3>
                    </div>
                    <br>
                    <br>
                </div>
                </div>
            </div>
        </div>
    </body>
    <footer>
        <div class="container">
            <div class="col-6">
                <img class="social-media" src="public/img/logo-inta.png">
                <img class="social-media" src="public/img/logo-face.png">
                <img class="social-media" src="public/img/logo-likendin.png">
            </div>
        </div>
    </footer>
</html>


