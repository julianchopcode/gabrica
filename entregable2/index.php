<?php
/**
* Template Name: Gabrica WP
* 
*/
<?php get_header(); ?>
<div class="container">
    <h1><?= the_title(); ?></h1>
        <div class="row"> 
            <div class="col-12">
            <img src="public/img/header.png" alt="">
            </div>
        </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-xs-12">
            <?= the_content(); ?>
        </div>
        <div class="col-lg-6 col-md-6 col-xs-12">
            <img src="public/img/contentright.png" alt="">
        </div>
    </div>
</div>
<?php get_sidebar(); ?>
<?php get_footer(); ?>
